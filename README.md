# install-terraform Ansible Role

This role has been developed specifically for [Debian GNU/Linux](https://debian.org) to
install [Terraform](https://www.terraform.io/) following the official
[installation guide](https://learn.hashicorp.com/tutorials/terraform/install-cli).

## Quick start

### Pull in the role to your system

Add a proper dependency to your project. Create -- if it doesn't exist yet -- a file called
`requirements.yaml`. Then add the dependency to it. The file should look something like
this:

```yaml
---
roles:
  # Potentiall some other roles that you might be using
  # ...
  - src: https://gitlab.com/Prezu/install-terraform.git
  # And maybe some other roles here (the order doesn't matter).
```

Having done that, use `ansible-galaxy` to pull the role:

```bash
$ ansible-galaxy install -r requirements.yaml
```

This is a one-time thing. Once you pull the role it, it'll live in your `~/.ansible/roles`
subdirectory.

## Using the role in a playbook

The role requires only one variable to be set: `debian_release`. Set it to the release,
you're using (e.g. `buster`, `bullseye`, `bookworm`). E.g:

```yaml
# ...
roles:
  # Whatever roles you've been using so far.
  # ...
  - role: install-terraform
    vars:
      debian_release: bullseye
  # ...
```
